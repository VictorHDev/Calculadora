#include <iostream>
#include <stdio.h> //gets();
#include <stdlib.h> //EXIT_FAILURE
#include <math.h> //pow()
#include <windows.h> //Sleep()
using namespace std;

//Escopo das funções
int fatorial(int n);
int fibo(int n);
void menu(char *esc);
int poten(int consc , int expo);
int denovo();
int main();

//MENU - Selecionar o tipo da operação
void menu(char *esc){
    std::cout << "\t\t\t\t\t\tMenu" << '\n';
    std::cout << "\t\t\tQual operação deseja fazer? Digite a letra dela (em CAPSLOCK)" << '\n';
    std::cout << "\t\t\tA - Adicao\t\t\t\tF - Fatorial" << '\n';
    std::cout << "\t\t\tS - Substracao\t\t\t\tB - Fibonnacci" << '\n';
    std::cout << "\t\t\tM - Multiplicacao\t\t\tP - Potencia" << '\n';
    std::cout << "\t\t\tD - Divisao";
    std::cout << "" << '\n';
    std::cout << "\t\t\tLetra da Operacao:";
    std::cin >> esc;
}

int denovo(){
    char sn;
    std::cout << "" << '\n';
    Sleep(500);
    escolha:
    std::cout << "Deseja realizar outra operação? [S/N]: ";
    std::cin >> sn;
    if (sn=='n' or sn=='N') {
        exit (EXIT_FAILURE);
    }else if(sn=='s' or sn=='S'){
        system("cls");
        main();
    }else{
        std::cout << "Resposta invalida!" << '\n';
        goto escolha;
    }
}

int main() {
    char esc = 'a';
    menu(&esc);

    system("cls");

    int val,res,n1,n2,confer,base,expo;

    //Levar para a função correspondente de cada função
    switch (esc){
        case 'A':
            system("cls");
            std::cout << "Insira os numeros a serem adicionados: " << '\n';
            std::cout << "Numero 1: ";
            std::cin >> n1;
            std::cout << "Numero 2: ";
            std::cin >> n2;
            res=n1+n2;
            std::cout << "" << '\n';
            std::cout << "R: " << res << '\n';
            denovo();
        case 'S':
            subt:
            system("cls");
            std::cout << "Insira os numeros a serem subtraidos: " << '\n';
            std::cout << "Numero 1: ";
            std::cin >> n1;
            std::cout << "Numero 2: ";
            std::cin >> n2;
            if(n1<n2){
                std::cout << "O Numero 1 precisa ser maior que o segundo." << '\n';
                Sleep(2000);
                goto subt;
            }
            res=n1-n2;
            std::cout << "" << '\n';
            std::cout << "R: " << res << '\n';
            denovo();
        case 'D':
            system("cls");
            std::cout << "Insira os numeros a serem multiplicados: " << '\n';
            std::cout << "Numero 1: ";
            std::cin >> n1;
            std::cout << "Numero 2: ";
            std::cin >> n2;
            res=n1/n2;
            std::cout << "" << '\n';
            std::cout << "R: " << res << '\n';
            denovo();
        case 'M':
            std::cout << "Numero 1:";
            std::cin >> n1;
            std::cout << "Numero 2:";
            std::cin >> n2;
            res=n1*n2;
            std::cout << "" << '\n';
            std::cout << "R: " << res << '\n';
            denovo();
        case 'F':
            system("cls");
            std::cout << "Valor a ser fatorado:" << '\n';
            std::cin >> n1;
            res=fatorial(n1);
            std::cout << "R:" << res << '\n';
            denovo();
        case 'B':
            system("cls");
            std::cout << "Total de valores:" << '\n';
            std::cin >> n1;
            std::cout << "" << '\n';
            for (int i=0; i<n1; i++) {
                std::cout << fibo(i+1) << " ";
            }
            denovo();
        case 'P':
            system("cls");
            std::cout << "Base:";
            std::cin >> base;
            std::cout << "Expoente:";
            std::cin >> expo;
            cout << pow(base,expo);
            denovo();
        default:
            system("cls");
            std::cout << "Dont expected." << '\n';
            denovo();
    }

    return 0;
}

int fatorial(int n){
    if (n==0){
        return 1;
    }
    return n*fatorial(n-1);
}

int fibo(int n){
    if (n==1 or n==2){
        return 1;
    }else{
        return fibo(n-1)+fibo(n-2);
    }
}
